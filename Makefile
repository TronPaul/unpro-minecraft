cloud-init/no-cloud.iso: cloud-init/meta-data cloud-init/user-data
	mkisofs -joliet -rock -volid "cidata" -output cloud-init/nocloud.iso cloud-init/meta-data cloud-init/user-data

cloud-init/user-data: minecraft-module/cloud-init minecraft-module/install-minecraft.sh
	cd cloud-init && ./make-mime.py ../minecraft-module/cloud-init:cloud-config ../minecraft-module/install-minecraft.sh:x-shellscript > user-data

clean:
	rm cloud-init/nocloud.iso cloud-init/user-data

script-release:
	tar -C scripts/ -czf mc-scripts.tar.gz .
